import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  theme:any;

  headerColor 
  borderColor 
  headingColor 
  headingFont 
  mainColor 
  contentFont
  contentColor
  contentSize
  bannerImage
  logoColor
  menuColor

  theme1:any = {
    headerColor: "rgb(20, 107, 171)",
    borderColor:  "#A21FA2",
    headingColor:  "rgb(20, 107, 171)",
    headingFont:  "serif",
    mainColor:  "rgb(219, 240, 245)",
    contentFont:  "serif",
    contentColor:  "#000000",
    contentSize:  "15px",
    bannerImage:  "url('http://blockchainconsult.co/wp-content/themes/blade/images/ico/ico_bnr.jpg')",
    logoColor:  "#A21FA2",
    menuColor: "#ffffff",
  }

  theme2:any = {
    headerColor: "#A21FA2",
    borderColor:  "rgb(20, 107, 171)",
    headingColor:  "#A21FA2",
    headingFont:  "Monospaced",
    mainColor:  "rgb(236, 219, 245)",
    contentFont:  "Monospaced",
    contentColor:  "#000000",
    contentSize:  "15px",
    logoColor:  "rgb(20, 107, 171)",
    menuColor: "#ffffff"
  }

  


  
  
  constructor(){
    // var test = document.getElementById('test1');
    // console.log('test: ', test);
  }

  ngOnInit(){
    var test = document.getElementById('test1');
    console.log('test: ', test);

    this.theme = this.theme1
  
    this.headerColor = this.theme.headerColor
    this.borderColor = this.theme.borderColor
    this.headingColor = this.theme.headingColor
    this.headingFont = this.theme.headingFont
    this.mainColor = this.theme.mainColor
    this.contentFont = this.theme.contentFont
    this.contentColor = this.theme.contentColor
    this.contentSize = this.theme.contentSize
    this.bannerImage = this.theme.bannerImage
    this.logoColor = this.theme.logoColor
    this.menuColor = this.theme.menuColor
  }
}
